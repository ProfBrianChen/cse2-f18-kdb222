///// Ken Brea CSE2 Class of 2022
/// 09/07/2018
// This program is meant to measure speed and distance through a cyclometer.
public class Cyclometer{
    	// main method required for every Java program
   	public static void main(String[] args) {
      int secsTrip1=480; // the number of seconds for trip 1
      int secsTrip2=3220; // The number of seconds for trip 2
		  int countsTrip1=1561; // the number of counts for trip 1
		  int countsTrip2=9037; //  the number of counts for trip 2
        double wheelDiameter=27.0,  //
  	  PI=3.14159, // PI is needed to compute diameter.
  	  feetPerMile=5280,  // Important Conversions
  	  inchesPerFoot=12,   // ^
  	  secondsPerMinute=60;  // ^
	  double distanceTrip1, distanceTrip2,totalDistance;  // 
System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
    // Trip 1 took 8.0 minutes and had 1561 counts
		// Trip 2 took 53.6 minutes and had 9037 counts
      // Now we are going to compute how far the trips traveled.
	  distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	  totalDistance=distanceTrip1+distanceTrip2;
      //Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");


	

	}  //end of main method 
	
} 
