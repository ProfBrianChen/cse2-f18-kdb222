////////////////// Ken Brea CSE2 10/18/2018
/////////////// Lab 06

import java.util.*;
/////// imports Scanner
public class NestedLoops{
	public static void main (String[] args){
		Scanner myScanner = new Scanner(System.in);
    System.out.println("How many rows do you want?");
    ////////// Asks user for how many rows of code they want
    int rows = myScanner.nextInt();
     ////////// User inputs the int variable
		for(int i = 0; i <= rows; i++){
			
				for(int t = 0; t <= i; t++){
					System.out.println("* ");
    ////////////// Will print out a pattern.
			}
      System.out.println();
		}

	}	
}