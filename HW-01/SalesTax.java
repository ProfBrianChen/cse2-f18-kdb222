//// Ken Brea CSE2 Homework 2
public class SalesTax{
  public static void main (String[] args){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax ratebe
    double paSalesTax = 0.06;
    ///////  Untaxed items
     double totalCostOfPants;   //total cost of pants

  System.out.println("This is the cost of pants without tax " + (numPants) * (pantsPrice));
  System.out.println("This is the cost of belts without tax " + (numBelts) * (beltCost));
  System.out.println("This is the cost of shirts without tax " + (numShirts) * (shirtPrice));
    /////// Items without tax
  System.out.println("This is the cost of pants with tax " + ((pantsPrice * paSalesTax) + (numPants) * (pantsPrice)));
  System.out.println("This is the cost of belts with tax " + ((beltCost * paSalesTax) + (numBelts) * (beltCost)));
  System.out.println("This is the cost of shirts " + ((shirtPrice * paSalesTax) + (numShirts) * (shirtPrice)));
    ////// Items with tax added
  System.out.println("This is the total of the untaxed items " + ((numShirts * shirtPrice) + (numBelts * beltCost) + (numPants * pantsPrice)));
  System.out.println("This is the total of all the items including tax " + ((shirtPrice * paSalesTax) + (numShirts) * (shirtPrice) + ((beltCost * paSalesTax) + (numBelts) * (beltCost) + (pantsPrice * paSalesTax) + (numPants) * (pantsPrice))));
    ////// The complete cost of all the items.
  }
    
}