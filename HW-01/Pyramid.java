import java.util.Scanner;
  public class Pyramid {
    public static void main (String[] args) {
      Scanner myScanner = new Scanner ( System.in );
        System.out.print("The square side of the Pyramid is (input length) : ");
        double pyramidLength = myScanner.nextDouble();
        System.out.println("The height of the Pyramid is (input height) : ");
         double pyramidHeight = myScanner.nextDouble();
         double pyramidVol;
         double pyramidArea;
         pyramidArea = (pyramidLength) * (pyramidHeight);
         pyramidVol = (2 * (pyramidHeight) * (pyramidArea) / 3);
         System.out.println("The volume of the pyramid is : " + (pyramidVol));
        
    }
  } 
   