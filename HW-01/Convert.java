////////////////////////// Ken Brea CSE 2 9/14/18
import java.util.Scanner;
///////////// Importing Scanner class needed.
  public class Convert {
     public static void main (String[] args) {
       Scanner myScanner = new Scanner ( System.in );
       ///////////////////// Declaring my Scanner
        System.out.print("Enter the affected area in acres ");
        double areaAcres = myScanner.nextDouble();
        System.out.println("Enter the amount of inches of rain");
        double inchesRain = myScanner.nextDouble();
        double cubicMiles;
        double gallons;
        gallons = ((inchesRain) * (areaAcres)) * 27154.285990761;
       //////////// Conversion for acre-inch to gallons
        cubicMiles = (gallons) * 9.08169e-13;
       ///////////// The conversion for gallons to cubic miles.
        System.out.println("That is " + (cubicMiles) + " cubic miles" );
        
        
     }
  }
 