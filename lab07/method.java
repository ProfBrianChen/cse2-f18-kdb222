import java.util.*;
public class method {
  public static void main (String[] args) {
    Random randomGenerator = new Random();
      int randomInt = randomGenerator.nextInt(10);
    String a = Adjectives(randomInt);
    String b = SubjectNouns(randomInt);
    String c = ObjectNouns(randomInt);
    String d = Verbs(randomInt);
    System.out.println(" The " + a + b + d + c);
          
   
    }
  
  public static String Adjectives(int randomInt){
    String Adjectives = "";
      if(randomInt == 1 ){
        Adjectives = "Strong";
      }
      else if(randomInt == 2){
         Adjectives = "Weak";
      }
      else if(randomInt == 3){
        Adjectives = "Fast";
      }
      else if (randomInt == 4){
        Adjectives = "Slow";
      }   
      else if (randomInt == 5){
        Adjectives = "Big";
      }    
      else if (randomInt == 6){
        Adjectives = "Small";
      } 
      else if (randomInt == 7){
        Adjectives = "Annoying";
      }  
      else if (randomInt == 8){
        Adjectives = "Smart";
      }
      else if (randomInt == 9){
        Adjectives = "Dumb";
      }  
      else if (randomInt == 10){
        Adjectives = "Tall";
      }
  
  
      return Adjectives;
  }
  	public static String SubjectNouns(int randomInt){
  		String SubjectNouns = " ";
  		if(randomInt == 1 ){
        SubjectNouns = " Dog";
      }
      else if(randomInt == 2){
         SubjectNouns = " Cat";
      }
      else if(randomInt == 3){
        SubjectNouns = " Boy";
      }
      else if (randomInt == 4){
        SubjectNouns = " Girl";
      }   
      else if (randomInt == 5){
        SubjectNouns = " Man";
      }    
      else if (randomInt == 6){
        SubjectNouns = " Fox";
      } 
      else if (randomInt == 7){
        SubjectNouns= " Turtle";
      }  
      else if (randomInt == 8){
        SubjectNouns = " Hare";
      }
      else if (randomInt == 9){
        SubjectNouns = " Bunny";
      }  
      else if (randomInt == 10){
       SubjectNouns = " Woman";
      }
      return SubjectNouns;
    }
  public static String ObjectNouns(int randomInt){
  		String ObjectNouns = " ";
  		if(randomInt == 1 ){
        ObjectNouns = " The Floor";
      }
      else if(randomInt == 2){
         ObjectNouns = " The Rock";
      }
      else if(randomInt == 3){
        ObjectNouns = " The Lake";
      }
      else if (randomInt == 4){
        ObjectNouns = " The Pool";
      }   
      else if (randomInt == 5){
        ObjectNouns = " The Stone";
      }    
      else if (randomInt == 6){
        ObjectNouns = " The Bone";
      } 
      else if (randomInt == 7){
        ObjectNouns= " The Sand";
      }  
      else if (randomInt == 8){
        ObjectNouns = " The Chair";
      }
      else if (randomInt == 9){
        ObjectNouns = " The Booth";
      }  
      else if (randomInt == 10){
       ObjectNouns = " The Cable";
      }
      return ObjectNouns;
    }
  public static String Verbs(int randomInt){
  		String Verbs = " ";
  		if(randomInt == 1 ){
        Verbs = " Ran";
      }
      else if(randomInt == 2){
         Verbs = " Jumped";
      }
      else if(randomInt == 3){
        Verbs = " Took";
      }
      else if (randomInt == 4){
        Verbs = " Walked";
      }   
      else if (randomInt == 5){
        Verbs = " Picked";
      }    
      else if (randomInt == 6){
        Verbs = " Climbed";
      } 
      else if (randomInt == 7){
        Verbs = " Grasped";
      }  
      else if (randomInt == 8){
        Verbs = " Broke";
      }
      else if (randomInt == 9){
        Verbs = " Ate";
      }  
      else if (randomInt == 10){
       Verbs = " Made";
      }
      return Verbs;
    }
}